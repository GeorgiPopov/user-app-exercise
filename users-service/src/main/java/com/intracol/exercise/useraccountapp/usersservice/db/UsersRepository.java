package com.intracol.exercise.useraccountapp.usersservice.db;

import com.intracol.exercise.useraccountapp.commonservice.models.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsersRepository extends CrudRepository<AppUser, UUID> {
  AppUser findByUsername(String username);
}
