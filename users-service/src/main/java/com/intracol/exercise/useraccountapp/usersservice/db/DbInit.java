package com.intracol.exercise.useraccountapp.usersservice.db;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppRoleDTO;
import com.intracol.exercise.useraccountapp.commonservice.models.AppRole;
import com.intracol.exercise.useraccountapp.commonservice.models.AppUser;
import com.intracol.exercise.useraccountapp.usersservice.services.UsersService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DbInit implements CommandLineRunner {
  private final UsersService service;

  public DbInit(UsersService service) {
    this.service = service;
  }

  @Override
  public void run(String... args) {
    // Delete all
    this.service.deleteAll();

    // Crete users
    AppUser admin = new AppUser();
    admin.setUsername("admin");
    admin.setPassword("12345");
    admin.setName("admin name");
    admin.setEmail("admin@email.com");
    admin.setAddress("admin address");
    AppRole test = new AppRole();
    test.setId(UUID.fromString("117ceb2f-8071-4ca4-a4b6-7df32973166c"));
    admin.setRole(test);
    Map<String, String> properties = new HashMap<>();
    properties.put("custom field 1", "value1");
    properties.put("custom field 2", "value2");
    properties.put("custom field 3", "value3");
    admin.setProperties(properties);
    Optional<AppUser> savedAdmin = service.insert(admin);
    AppUser user = new AppUser();
    user.setUsername("gpopov");
    user.setPassword("12345");
    user.setName("gpopov name");
    user.setEmail("gpopov@email.com");
    user.setAddress("gpopov address");
//    user.setRole("USER");
    Map<String, String> properties1 = new HashMap<>();
    properties1.put("custom field 11", "value11");
    properties1.put("custom field 21", "value21");
    properties1.put("custom field 31", "value31");
    user.setProperties(properties1);
    Optional<AppUser> savedUser = service.insert(user);

    List<AppUser> users = Arrays.asList(savedAdmin.get(), savedUser.get());

    // Save in to DB
    this.service.saveAll(users);
  }

}
