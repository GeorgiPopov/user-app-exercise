package com.intracol.exercise.useraccountapp.usersservice.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties("default-user")
public class Configuration {
    private String username;
    private String name;
    private String email;
    private String address;
    private Map<Integer, Object> customData = new HashMap<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<Integer, Object> getCustomData() {
        return customData;
    }

    public void setCustomData(Map<Integer, Object> customData) {
        this.customData = customData;
    }
}
