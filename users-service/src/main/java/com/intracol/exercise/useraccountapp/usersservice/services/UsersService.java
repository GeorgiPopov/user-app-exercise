package com.intracol.exercise.useraccountapp.usersservice.services;

import com.intracol.exercise.useraccountapp.usersservice.db.UsersRepository;
import com.intracol.exercise.useraccountapp.commonservice.models.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UsersService {

  @Autowired
  private UsersRepository repository;

  public List<AppUser> findAllUsers() {
    return (List<AppUser>) repository.findAll();
  }

  public Optional<AppUser> findById(UUID id) {
    return repository.findById(id);
  }

  public Optional<AppUser> findByUsername(String name) {
    AppUser result = repository.findByUsername(name);
    if (result == null) {
      return Optional.empty();
    }
    return Optional.of(result);
  }

  public Optional<AppUser> insert(AppUser appUser) {
    if (existsById(appUser.getId()) || existByName(appUser.getName())) {
      return Optional.empty();
    }
    AppUser saved = repository.save(appUser);
    return Optional.of(saved);
  }

  public boolean delete(UUID id) {
    if (!existsById(id)) {
      return false;
    }
    repository.deleteById(id);
    return true;
  }

  public Optional<AppUser> update(AppUser appUser) {
    if (!existByName(appUser.getName())) {
      return Optional.empty();
    }
    AppUser updated = repository.save(appUser);
    return Optional.of(updated);
  }

  public void deleteAll() {
    try {
      repository.deleteAll();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public void saveAll(List<AppUser> roles) {
    try {
      repository.saveAll(roles);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private boolean existsById(UUID id) {
    if (null == id) {
      return false;
    }
    return repository.existsById(id);
  }

  private boolean existByName(String name) {
    return repository.findByUsername(name) != null;
  }
}
