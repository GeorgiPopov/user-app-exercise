package com.intracol.exercise.useraccountapp.usersservice.controllers;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppUserDTO;
import com.intracol.exercise.useraccountapp.usersservice.configurations.Configuration;
import com.intracol.exercise.useraccountapp.commonservice.models.AppUser;
import com.intracol.exercise.useraccountapp.usersservice.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class UsersController {

  @Autowired
  private Configuration configuration;
  @Autowired
  private UsersService service;
  private Environment environment;

  @GetMapping
  public String home() {
    return "Hello from Gallery Service running at port: " + environment.getProperty("local.server.port");
  }

  @GetMapping("/all")
  public List<AppUser> getUsers() {
//    AppUser user1 = new AppUser();
//    user1.setUsername(configuration.getUsername());
//    user1.setName(configuration.getName());
//    user1.setEmail(configuration.getEmail());
//    user1.setAddress(configuration.getAddress());
//    user1.setRole("ADMIN");
//    AppUser user2 = new AppUser();
//    user2.setUsername("john");
//    user2.setName("john doe");
//    user2.setEmail("john-doe@mail.com");
//    user2.setAddress("Sofia, Bulgaria");
//    user2.setRole("USER");
//    List<AppUser> users = List.of(user1, user2);
    return service.findAllUsers();
  }

  @GetMapping("/{id}")
  public AppUser getUserById(@PathVariable String id) {
    return new AppUser();
  }

  @PostMapping
  public ResponseEntity<AppUser> createAppUser(@RequestBody AppUser user) {
    try {
      if (user != null) {
        Optional<AppUser> saved = service.insert(user);
        if (saved.isPresent()) {
          return new ResponseEntity<>(saved.get(), HttpStatus.CREATED);
        } else {
          throw new Exception("AppUser creation failed.");
        }
      } else {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/auth/{username}")
  public ResponseEntity<AppUserDTO> getUserForAuthentication(@PathVariable String username) {
    try {
      Optional<AppUser> user = service.findByUsername(username);
      if (user.isPresent()) {
        AppUser appUser = user.get();
        AppUserDTO respUser = new AppUserDTO();
        respUser.setName(appUser.getName());
        respUser.setUsername(appUser.getUsername());
        respUser.setEmail(appUser.getEmail());
        respUser.setAddress(appUser.getAddress());
        respUser.setPassword(appUser.getPassword());
        respUser.setId(appUser.getId());
        respUser.setProperties(appUser.getProperties());
        respUser.setRole(appUser.getRole());
        return new ResponseEntity<>(respUser, HttpStatus.FOUND);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }
  // -------- Admin Area --------
  // This method should only be accessed by users with role of 'admin'
  // We'll add the logic of role based auth later
  @RequestMapping("/admin")
  public String homeAdmin() {
    return "This is the admin area of Gallery service running at port: " + environment.getProperty("local.server.port");
  }
}
