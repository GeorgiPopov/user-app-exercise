package com.intracol.exercise.useraccountapp.limitsservice.controllers;

import com.intracol.exercise.useraccountapp.limitsservice.configuration.Configuration;
import com.intracol.exercise.useraccountapp.limitsservice.models.Limits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/limits")
public class LimitsController {

    @Autowired
    private Configuration configuration;

    @GetMapping
    public Limits retrieveLimits() {
        return new Limits(configuration.getMinimum(), configuration.getMaximum());
//        return  new Limits(1, 1000);
    }
}
