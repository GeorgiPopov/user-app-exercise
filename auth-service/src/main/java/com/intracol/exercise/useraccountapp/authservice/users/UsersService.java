package com.intracol.exercise.useraccountapp.authservice.users;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppUserDTO;
import com.intracol.exercise.useraccountapp.commonservice.services_communication.ServicesURIs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.*;

@Component
public class UsersService {
  @LoadBalanced
  private RestTemplate restTemplate;

  @Autowired
  public UsersService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public Optional<AppUserDTO> readById(UUID id) {
    URI uri = URI.create(ServicesURIs.USERS_SERVICE_BASE_URI + id);
    AppUserDTO authAppUser = restTemplate.getForObject(uri, AppUserDTO.class);
    return Optional.ofNullable(authAppUser);
  }

  public Optional<AppUserDTO> readByUsername(String username) {
    URI uri = URI.create(ServicesURIs.USERS_SERVICE_BASE_URI_AUTH + username);
    ResponseEntity<AppUserDTO> userResponseEntity = restTemplate.getForEntity(uri, AppUserDTO.class);
    AppUserDTO authAppUser = userResponseEntity.hasBody() ? userResponseEntity.getBody() : null;
    return Optional.ofNullable(authAppUser);
  }
}
