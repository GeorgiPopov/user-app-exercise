package com.intracol.exercise.useraccountapp.rolesservice.db;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppRoleDTO;
import com.intracol.exercise.useraccountapp.rolesservice.services.RolesService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.*;

  @Service
  public class DbInit implements CommandLineRunner {
    private final RolesService service;

    public DbInit(RolesService service) {
      this.service = service;
    }

    @Override
    public void run(String... args) {
      // Delete all
//      this.service.deleteAll();

      // Crete users
      AppRoleDTO admin = new AppRoleDTO();
      admin.setName("ADMIN");
      admin.setId(UUID.randomUUID());
      AppRoleDTO user = new AppRoleDTO();
      user.setId(UUID.randomUUID());
      user.setName("USER");
      // Save in to DB
//      this.service.insert(admin);
//      this.service.insert(user);
    }

  }
