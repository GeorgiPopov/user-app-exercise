package com.intracol.exercise.useraccountapp.rolesservice.services;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppRoleDTO;
import com.intracol.exercise.useraccountapp.rolesservice.db.RolesRepository;
import com.intracol.exercise.useraccountapp.commonservice.models.AppRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RolesService {

  @Autowired
  private RolesRepository repository;

  public List<AppRoleDTO> findAllRoles() {
    List<AppRole> all = (List<AppRole>) repository.findAll();
    List<AppRoleDTO> roleDTOS = new ArrayList<>();
    for (AppRole appRole : all) {
      String name = appRole.getName();
      UUID id = appRole.getId();
      AppRoleDTO roleDTO = new AppRoleDTO(id, name);
      roleDTOS.add(roleDTO);
    }
    return roleDTOS;
  }

  public Optional<AppRoleDTO> findByName(String name) {
    AppRole byName = repository.findByName(name);
    AppRoleDTO roleDTO = new AppRoleDTO(byName.getId(), byName.getName());
    return Optional.of(roleDTO);
  }

  public Optional<AppRoleDTO> insert(AppRoleDTO newRoleDTO) {
    AppRole newRole = new AppRole();
    newRole.setName(newRoleDTO.getName());
//    newRole.setId(newRoleDTO.getId());
    newRole = repository.save(newRole);
    newRoleDTO.setId(newRole.getId());
    return Optional.of(newRoleDTO);
  }

  public void deleteAll() {
    repository.deleteAll();
  }
}
