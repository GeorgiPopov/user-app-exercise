package com.intracol.exercise.useraccountapp.rolesservice.db;

import com.intracol.exercise.useraccountapp.commonservice.models.AppRole;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface RolesRepository extends CrudRepository<AppRole, UUID> {
  AppRole findByName(String name);
}
