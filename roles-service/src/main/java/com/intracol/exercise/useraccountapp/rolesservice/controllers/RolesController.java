package com.intracol.exercise.useraccountapp.rolesservice.controllers;

import com.intracol.exercise.useraccountapp.commonservice.dto.AppRoleDTO;
import com.intracol.exercise.useraccountapp.rolesservice.services.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class RolesController {

  @Autowired
  private RolesService service;

  @GetMapping("/all")
  public List<AppRoleDTO> getAllRoles() {
    return service.findAllRoles();
  }

  @GetMapping("{roleName}")
  public ResponseEntity<AppRoleDTO> getRoleByName(@PathVariable String roleName) {
    try {
      Optional<AppRoleDTO> byName = service.findByName(roleName);
      if (byName.isPresent()) {
        return new ResponseEntity<>(byName.get(), HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

  }

  @PostMapping
  public ResponseEntity<AppRoleDTO> createAppRole(@RequestBody AppRoleDTO roleDTO) {
    try {
      if (roleDTO != null) {
        Optional<AppRoleDTO> saved = service.insert(roleDTO);
        if (saved.isPresent()) {
          return new ResponseEntity<>(saved.get(), HttpStatus.OK);
        } else {
          throw new Exception("AppRole creation failed.");
        }
      } else {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }


}
