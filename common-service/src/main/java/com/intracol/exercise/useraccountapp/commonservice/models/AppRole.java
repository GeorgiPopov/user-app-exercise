package com.intracol.exercise.useraccountapp.commonservice.models;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "Role")
@Table(name = "roles")
public class AppRole {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "role_id")
  private UUID id;
  @Column(name = "name")
  private String name;

  public AppRole() {
    super();
  }

  public UUID getId() {
    return this.id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Role: { \"id\": ");
    builder.append(this.id).append(", ");
    builder.append("name: ").append(this.name).append(", ");
    builder.append(System.lineSeparator());

    return builder.toString();
  }
}
