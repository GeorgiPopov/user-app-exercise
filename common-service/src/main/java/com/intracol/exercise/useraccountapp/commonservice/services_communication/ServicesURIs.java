package com.intracol.exercise.useraccountapp.commonservice.services_communication;

public class ServicesURIs {
  static public final String USERS_SERVICE_BASE_URI = "http://users-service/";
  public static final String USERS_SERVICE_BASE_URI_AUTH = USERS_SERVICE_BASE_URI + "auth/";
}
