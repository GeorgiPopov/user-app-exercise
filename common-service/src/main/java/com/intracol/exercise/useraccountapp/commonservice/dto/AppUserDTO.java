package com.intracol.exercise.useraccountapp.commonservice.dto;

import com.intracol.exercise.useraccountapp.commonservice.models.AppRole;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AppUserDTO {

  private UUID id;
  private String username;
  private String password;
  private String name;
  private String email;
  private String address;
  private AppRole role;
  private Map<String, String> properties = new HashMap<>();

  public AppUserDTO() {
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public AppRole getRole() {
    return role;
  }

  public void setRole(AppRole role) {
    this.role = role;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }
}
