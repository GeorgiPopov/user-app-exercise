package com.intracol.exercise.useraccountapp.commonservice.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity(name = "User")
@Table(name = "users")
@TypeDefs({
  @TypeDef(name = "json", typeClass = JsonStringType.class),
  @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class AppUser {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "user_id")
  private UUID id;
  @Column(name = "username")
  private String username;
  @Column(name = "password")
  private String password;
  @Column(name = "name")
  private String name;
  @Column(name = "email")
  private String email;
  @Column(name = "address")
  private String address;
  @JsonBackReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "role_id", referencedColumnName= "role_id")
  private AppRole role;
  @Type(type = "jsonb")
  @Column(columnDefinition = "jsonb",
    name = "properties")
  private Map<String, String> properties = new HashMap<>();

  public AppUser() {
    super();
  }

  public UUID getId() {
    return this.id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public AppRole getRole() {
    return this.role;
  }

  public void setRole(AppRole role) {
    this.role = role;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("User: { \"id\": ");
    builder.append(this.id).append(", ");
    builder.append("username: ").append(this.username).append(", ");
    builder.append("name: ").append(this.name).append(", ");
    builder.append("address: ").append(this.address).append(", ");
    builder.append("properties: ").append(this.properties).append(", ");
    builder.append("email: ").append(this.email).append(" }");
    builder.append(System.lineSeparator());

    return builder.toString();
  }
}
